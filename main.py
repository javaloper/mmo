from generator import samples
from tester import tests
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
np.set_printoptions(threshold=np.nan)

generator = samples.SamplesGenerator()
# test poprawności generatora:
generator.test()

tester = tests.Tester()

# słownik dla pd do poźniejszego eksportu do pliku CSV
# d = {'matrix': [], 'acc1_1': [], 'acc1_2': [], 'acc2_1': [], 'acc2_2': []}
d = {'matrix': [], 'acc1_1': [], 'acc1_2': [], 'acc1_3': [], 'acc1_4': [], 'acc2_1': [], 'acc2_2': [], 'acc2_3': [], 'acc2_4': [], 'acc3_1': [], 'acc3_2': [], 'acc3_3': [], 'acc3_4': []}

for i in range(1, 10):
    for j in range(1, 10):
        x = 0.1*i
        y = 0.1*j
        matrix=np.array([[x, 1-x],[y, 1-y]])

        generator.set_trans_matrix(matrix)
        print(generator.get_trans_matrix())
        [o1x, o1y, o2x, o2y] = generator.generate_samples(n_samples = 2000)
        o2y = o2y.flatten()

        test1_1 = tester.test_on_data(o2x, o2y, k=2) #test uczenia ann dla obiektwow aktualnych
        test1_2 = tester.test_on_data(np.append(o1x, o2x, axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią argumentow
        test1_3 = tester.test_on_data(np.append(o1y, o2x, axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy
        test1_4 = tester.test_on_data(np.concatenate((o1y, o1x, o2x), axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy i argumentow

        test2_1 = tester.test_on_data_naive_bayes(o2x, o2y, k=2) # test uczenia bayesa dla ob. aktualnych
        test2_2 = tester.test_on_data_naive_bayes(np.append(o1x, o2x, axis=1), o2y, k=2) # test uczenia bayesa dla ob z hist. argumentow
        test2_3 = tester.test_on_data_naive_bayes(np.append(o1y, o2x, axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy
        test2_4 = tester.test_on_data_naive_bayes(np.concatenate((o1y, o1x, o2x), axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy i argumentow

        test3_1 = tester.test_on_data_svm(o2x, o2y, k=2) # test uczenia SVM dla ob. aktualnych
        test3_2 = tester.test_on_data_svm(np.append(o1x, o2x, axis=1), o2y, k=2) # test uczenia SVM dla ob z hist. argumentow
        test3_3 = tester.test_on_data_svm(np.append(o1y, o2x, axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy
        test3_4 = tester.test_on_data_svm(np.concatenate((o1y, o1x, o2x), axis=1), o2y, k=2) #test uczenia dla obiektow wraz z historią klasy i argumentow

        # kolejny wiersz danych dla pliku CSV:
        d['matrix'].append(np.array2string(matrix))
        d['acc1_1'].append(test1_1)
        d['acc1_2'].append(test1_2)
        d['acc1_3'].append(test1_3)
        d['acc1_4'].append(test1_4)
        d['acc2_1'].append(test2_1)
        d['acc2_2'].append(test2_2)
        d['acc2_3'].append(test2_3)
        d['acc2_4'].append(test2_4)
        d['acc3_1'].append(test3_1)
        d['acc3_2'].append(test3_2)
        d['acc3_3'].append(test3_3)
        d['acc3_4'].append(test3_4)

        print("----------ACCURACY SCORES:----------------------------")
        print("NN accuracy (obiekty aktualne): {}".format(test1_1))
        print("NN accuracy (obiekty z historią argumentow): {}".format(test1_2))
        print("NN accuracy (obiekty z historią klasy): {}".format(test1_3))
        print("NN accuracy (obiekty z historią klasy i argumentow): {}".format(test1_4))
        print("NB accuracy (obiekty aktualne): {}".format(test2_1))
        print("NB accuracy (obiekty z historią argumentow): {}".format(test2_2))
        print("NB accuracy (obiekty z historią klasy): {}".format(test2_3))
        print("NB accuracy (obiekty z historią klasy i argumentow): {}".format(test2_4))
        print("SVM accuracy (obiekty aktualne): {}".format(test3_1))
        print("SVM accuracy (obiekty z historią argumentow): {}".format(test3_2))
        print("SVM accuracy (obiekty z historią klasy): {}".format(test3_3))
        print("SVM accuracy (obiekty z historią klasy i argumentow): {}".format(test3_4))
        print("------------------------------------------------------")
        #print(tester.test_on_data(o2x, o2y, k=2)) #test uczenia ann dla obiektwow aktualnych
        #print(tester.test_on_data(np.append(o1y, o2x, axis=1), o2y, k=2))
        #print(tester.test_on_data(np.append(o1x, o2x, axis=1), o2y, k=2)) #test uczenia dla obiektow wraz z historią
        #print(tester.test_on_data(np.concatenate((o1y, o1x, o2x), axis=1), o2y, k=2))

df = pd.DataFrame(data=d)
df.to_csv('tester_scores.csv', sep=';')
print(df)
